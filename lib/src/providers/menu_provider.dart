import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';

class _MenuProvider {
  List<dynamic> opciones = [];

  _MenuProvider() {
    cargarData();
  }

  Future<List<dynamic>> cargarData() async {
    // carga el archivo json de la carpeta data y espera a que responda con await
    final resp = await rootBundle.loadString('data/menu_opts.json');
    // decodifica el archivo json y lo mapea en dataMap 
    Map dataMap = json.decode(resp);
    // print(dataMap['rutas']);
    opciones = dataMap['rutas'];

    return opciones;
  }
}
// Crea una instancia de menu provider publica para que pueda ser invocada desde otro page
final menuProvider = new _MenuProvider();


