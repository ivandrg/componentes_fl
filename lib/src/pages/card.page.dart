import 'package:flutter/material.dart';

class CardPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Cards Page'),
      ),
      body: ListView(
        // Agrega un margen a todos los lados del listview de 10
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
          _cardTipo1(),
          SizedBox(height: 30.0),
          _cardTipo2(),
          SizedBox(height: 30.0),
          _cardTipo3(),
          SizedBox(height: 30.0),
          _cardTipo1(),
          SizedBox(height: 30.0),
          _cardTipo2(),
          SizedBox(height: 30.0),
          _cardTipo3(),
          SizedBox(height: 30.0),
          _cardTipo1(),
          SizedBox(height: 30.0),
          _cardTipo2(),
          SizedBox(height: 30.0),
          _cardTipo3(),
        ],
      ),
    );
  }

  Widget _cardTipo1() {
    return Card(
      // Aplica sombra a la tarjeta
      elevation: 10.0,
      // Aplica el borde redondeado a la tarjeta
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      child: Column(
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.photo_album, color: Colors.blue),
            title: Text('Tarjeta 1'),
            subtitle: Text(
                'Esta es la tarjeta numero 1 para que ustedes la puedan observar'),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FlatButton(onPressed: () {}, child: Text('Cancelar')),
              FlatButton(onPressed: () {}, child: Text('OK')),
            ],
          )
        ],
      ),
    );
  }

  Widget _cardTipo2() {
    return Card(
      // Recorta todo lo que este dentro de la tarjeta para que se ajuste al tamaño forma de la misma
      clipBehavior: Clip.antiAlias,
      elevation: 10,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      child: Column(
        children: <Widget>[
          // Carga una imagen desde url y permite agregar una img de carga como placeholer
          FadeInImage(
            placeholder: AssetImage('assets/original.gif'),
            image: NetworkImage(
                'https://thewallpaper.co//wp-content/uploads/2016/10/neon-genesis-evangelion-hd-wallpapers-desktop-wallpapers-4k-high-definition-windows-10-mac-apple-colourful-images-download-wallpaper-free-1920x1080.jpg'),
            // Ajusta la duracion del placeholder
            fadeInDuration: Duration(milliseconds: 200),
            height: 300,
            // Permite ajustar la imagen al contorno de la columna
            fit: BoxFit.cover,
          ),

          // Inserta una imagen desde URL
          // Image(
          //     image: NetworkImage(
          //         'https://thewallpaper.co//wp-content/uploads/2016/10/neon-genesis-evangelion-hd-wallpapers-desktop-wallpapers-4k-high-definition-windows-10-mac-apple-colourful-images-download-wallpaper-free-1920x1080.jpg')
          //         ),

          // Inserta un container con un texto
          Container(
            // Genera margen al rededor del titulo
            padding: EdgeInsets.all(10.0),
            child: Text('Ajustes con clipBehavior'),
          )
        ],
      ),
    );
  }

  Widget _cardTipo3() {
// Asigna el objeto Card a la variable card
    final card = Container(
      child: Column(
        children: <Widget>[
          // Carga una imagen desde url y permite agregar una img de carga como placeholer
          FadeInImage(
            placeholder: AssetImage('assets/original.gif'),
            image: NetworkImage(
                'https://thewallpaper.co//wp-content/uploads/2016/10/neon-genesis-evangelion-hd-wallpapers-desktop-wallpapers-4k-high-definition-windows-10-mac-apple-colourful-images-download-wallpaper-free-1920x1080.jpg'),
            // Ajusta la duracion del placeholder
            fadeInDuration: Duration(milliseconds: 200),
            height: 300,
            // Permite ajustar la imagen al contorno de la columna
            fit: BoxFit.cover,
          ),

          // Inserta una imagen desde URL
          // Image(
          //     image: NetworkImage(
          //         'https://thewallpaper.co//wp-content/uploads/2016/10/neon-genesis-evangelion-hd-wallpapers-desktop-wallpapers-4k-high-definition-windows-10-mac-apple-colourful-images-download-wallpaper-free-1920x1080.jpg')
          //         ),

          // Inserta un container con un texto
          Container(
            // Genera margen al rededor del titulo
            padding: EdgeInsets.all(10.0),
            child: Text('Ajustes con Contenedor'),
          )
        ],
      ),
    );
    return Container(
      decoration: BoxDecoration(
          // Genera la forma del contenedor con bordes ciruculares
          borderRadius: BorderRadius.circular(30),
          // Color del contendor
          color: Colors.white,
          boxShadow: <BoxShadow>[
          // Genera sombra negra
            BoxShadow(
                color: Colors.black26,
                blurRadius: 10,
                spreadRadius: 2,
                offset: Offset(2, 10))
          ]),
      child: ClipRRect(
        // Recorta todo lo que este afuera de la forma del contenedor
        borderRadius: BorderRadius.circular(30),
        child: card,
      ),
    );
  }
}


