import 'package:flutter/material.dart';

class AvatarPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Avatar Page'),
        actions: <Widget>[
          Container(
            padding: EdgeInsets.all(5),
            // Widget para circulo de avatar
            child: CircleAvatar(
              // Carga imagen en el widget
              backgroundImage: NetworkImage(
                  'https://i2.wp.com/codigoespagueti.com/wp-content/uploads/2020/04/Asuka-Evangelion.jpg?resize=1200%2C720&quality=80&ssl=1'),
              // Radio de widget
              radius: 25,
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 10),
            child: CircleAvatar(
              child: Text('SL'),
              backgroundColor: Colors.brown,
            ),
          )
        ],
      ),
      body: Center(
        child: FadeInImage(
            placeholder: AssetImage('assets/original.gif'),
            fadeInDuration: Duration(milliseconds: 100),
            image: NetworkImage(
                'https://i.pinimg.com/originals/55/3c/6b/553c6b6d0eb40714c618572f801c1e5a.png')),
      ),
    );
  }
}


