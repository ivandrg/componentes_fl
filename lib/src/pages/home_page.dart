import 'package:flutter/material.dart';
import 'package:componentes/src/providers/menu_provider.dart';
import 'package:componentes/src/utils/icono_string_util.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Componentes'),
      ),
      body: _lista(),
    );
  }

  Widget _lista() {
    return FutureBuilder(
      // Future es el que se encarga de esperar la data de una fucion future
      future: menuProvider.cargarData(),
      // Datos iniciales con los que se carga el widget si no hay respuesta de la funcion
      initialData: [],
      // Es el que se encarga de almacenar la data del future como un snapshot
      builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
        // Se retorna el widget lista con la funcion listaItems y con la data del future
        return ListView(
          children: _listaItems(snapshot.data, context),
        );
      },
    );
  }

  List<Widget> _listaItems(List<dynamic> data, BuildContext context) {
    // Crea lista tipo widget vacia
    final List<Widget> opciones = [];
    // crea un ciclo para iterar en todos los objetos que tenga data
    data.forEach((opt) {
      // crea un widget  listtile donde se mostraran todos los objetos de data
      final widgetTemp = ListTile(
        title: Text(opt['texto']),
        subtitle: Text(opt['texto']),
        leading: getIcon(opt['icon']),
        trailing: Icon(Icons.keyboard_arrow_right, color: Colors.blue),
        onTap: () {
          // Forma para navegar con routero este se llama desde el main
          Navigator.pushNamed(context, opt['ruta']);

// Forma larga para navegar sin routero
          // final route = MaterialPageRoute(builder: (context) {
          //   return AlertPage();
          // });
          // Navigator.push(context, route);
        },
      );
      // se agrega cada iteracion de listtile con la data a la lista opciones para ser visualizada con el divider
      opciones..add(widgetTemp)..add(Divider());
    });
    return opciones;
  }
}


