import 'package:flutter/material.dart';

class HomePageTemp extends StatelessWidget {
  final opciones = ['uno', 'Dos', 'Tres', 'Cuatro', 'Cinco'];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Componentes Temp'),
      ),
      body: ListView(
          // children: _crearItems()),
          children: _crearItemsCorta()),
    );
  }

// Mostrar lista forma larga
  List<Widget> _crearItems() {
// Crea una nueva lista vacia
    List<Widget> lista = new List<Widget>();
// Ciclo que muestra las variables en una lista opt son las variables dentro de la lista opciones
    for (String opt in opciones) {
      final tempWidget = ListTile(
        title: Text(opt),
      );
      // Agrega cada listtile con la varaible a la lista final
      lista
        ..add(tempWidget)
        ..add(Divider(
          color: Colors.redAccent,
          height: 50.0,
        ));
    }
    return lista;
  }

// Mostrar lista forma corta
  List<Widget> _crearItemsCorta() {
    // Retorna una lista con las variables de el string opciones
    // con la funcion map mapea todas las variables en el arreglo opciones
    return opciones.map((item) {
      // Se pone el colum para poder aplicar el divider
      return Column(
        children: <Widget>[
          // Muestra cada cajon de la lista con su propiedades iguales para cada elemento
          ListTile(
            title: Text(item + '!'),
            subtitle: Text('Cualquier cosa'),
            leading: Icon(Icons.account_balance_wallet),
            trailing: Icon(Icons.keyboard_arrow_right),
            onTap: (){},
          ),
          Divider()
        ],
      );
    }).toList();

     
  }
}

