import 'dart:math';

import 'package:flutter/material.dart';

class AnimatedContainerPage extends StatefulWidget {
  @override
  _AnimatedContainerPageState createState() => _AnimatedContainerPageState();
}

class _AnimatedContainerPageState extends State<AnimatedContainerPage> {
  double _width = 50.0;
  double _height = 50.0;
  Color _color = Colors.pink;
  BorderRadiusGeometry _borderRadius = BorderRadius.circular(8);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Animated Container'),
      ),
      body: Center(
        child: AnimatedContainer(
          // Duracion de la animacion
          duration: Duration(milliseconds: 600),
          // Curva de la animacion
          curve: Curves.bounceOut,
          width: _width,
          height: _height,
          // Border del contenedor y color
          decoration: BoxDecoration(borderRadius: _borderRadius, color: _color),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.change_history),
        onPressed: _cambiarForma,
      ),
    );
  }

  void _cambiarForma() {
    // Funcion random
    final random = Random();
    // Setea el estado del contenedor con medidas aleatorias
    setState(() {
      _height = random.nextInt(300).toDouble();
      _width = random.nextInt(300).toDouble();
      _color = Color.fromRGBO(
        random.nextInt(255), 
        random.nextInt(255), 
        random.nextInt(255), 
        1);
        _borderRadius = BorderRadius.circular(random.nextInt(100).toDouble());
    });
  }
}


