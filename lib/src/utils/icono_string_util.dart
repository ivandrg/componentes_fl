import 'package:flutter/material.dart';

// Crea un arreglo de strings con los iconos
final _icons = <String, IconData>{
  'add_alert'     :   Icons.add_alert,
  'accessibility' :   Icons.accessibility,
  'folder_open'   :   Icons.folder_open,
  'donut_large'   :   Icons.donut_large,
  'input'        :   Icons.input,
};

// Metodo que retorna el widget icon con el parametro que recibe la fucnion nombreIcono
Icon getIcon(String nombreIcono) {
  return Icon(_icons[nombreIcono], color: Colors.blue);
}


