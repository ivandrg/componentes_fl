import 'package:componentes/src/pages/alert_page.dart';
import 'package:componentes/src/routes/routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Componentes App',
      debugShowCheckedModeBanner: false,
// Para agregar la internacionalizacion
      localizationsDelegates: [
        // ... app-specific localization delegate[s] here
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en'), // English
        const Locale('es'), 

      ],
      // home: HomePage()
      // Asigna la ruta inicial como el home desde el routero
      initialRoute: '/',
      // Llama al metodo que envia la ruta de la pagina a la que debe navegar
      routes: getApplicationRoutes(),
      // Metodo para navegar entre paginas de manera automatica
      onGenerateRoute: (RouteSettings settings) {
        print('Ruta llamada: ${settings.name}');
        // Devuelve la ruta de pagina donde se va a navegar
        return MaterialPageRoute(
            builder: (BuildContext context) => AlertPage());
      },
    );
  }
}


